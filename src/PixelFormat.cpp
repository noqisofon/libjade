


PixelFormat::PixelFormat()
{
    format_ = SDL_GetVideoInfo()->vfmt;
}


Uint8 PixelFormat::getBitsPerPixel() const
{
    return format_->BitsPerPixel;
}


Uint8 PixelFormat::getBytesPerPixel() const
{
    return format_->BytesPerPixel;
}


Uint32 PixelFormat::getRMask() const
{
    return format_->Rmask;
}


Uint32 PixelFormat::getGMask() const
{
    return format_->Gmask;
}


Uint32 PixelFormat::getBMask() const
{
    return format_->Bmask;
}


Uint32 PixelFormat::getAMask() const
{
    return format_->Amask;
}
