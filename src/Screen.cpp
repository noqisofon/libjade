

void Screen::updateRect(Sint32 x, Sint32 y, Sint32 width, Sint32 height)
{
    SDL_UpdateRect( content_, x, y, width, height );
}


void Screen::flip()
{
    SDL_Flip( content_ );
}
