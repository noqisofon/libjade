#if defined(HAVE_CONFIG_H)
#   include "config.h"
#endif  /* defined(HAVE_CONFIG_H) */

#include <jade/Surface.hxx>
USING_NAMESPACE_CORE;
USING_NAMESPACE_JADE;


Surface::Surface(int width, int height, int depth, Uint32 flags)
    : content_(NULL), has_owner_(true)
{
    Uint32 rmask, gmask, bmask, amask;
#if SDL_BYTEORDER == SDL_BIG_ENDIAN
    rmask = 0xff000000;
    gmask = 0x00ff0000;
    bmask = 0x0000ff00;
    amask = 0x000000ff;
#else
    rmask = 0x000000ff;
    gmask = 0x0000ff00;
    bmask = 0x00ff0000;
    amask = 0xff000000;
#endif
    content_ = SDL_CreateRGBSurface( flags, width, height, depth, rmask, gmask, bmask, amask );
}


Surface::Surface(SDL_Surface* primitive_other, bool managed)
    : content_(primitive_other), has_owner_(managed)
{
}


Surface::~Surface()
{
}


bool Surface::blit(const Surface* const& dest, const Rect* const& rect, const Point* const& pos)
{
    SDL_Rect dest_rect = {
        rect->x,
        rect->y,
        rect->width,
        rect->height
    };
    SDL_Rect dest_pos = {
        pos->x,
        pos->y,
        0,
        0
    };
    return SDL_BlitSurface( content_, &dest_rect, &dest_pos ) == 0;
}


bool Surface::fillRect(Sint32 x, Sint32 y, Sint32 width, Sint32 height, Uint32 color);
{
    SDL_Rect rect = { x, y, width, height };

    return SDL_FillRect( content_, &rect, color ) == 0;
}
bool Surface::fillRect(Uint32 color)
{
    return SDL_FillRect( content_, NULL, color ) == 0;
}


void Surface::finalize()
{
    if ( has_owner_ )
        SDL_FreeSurface( content_ );
}
// Local Variables:
//   coding: utf-8
// End:
