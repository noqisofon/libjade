#include <core/Releasable.hxx>
USING_NAMESPACE_CORE;

Releasable::Releasable()
    : ref_count_( new unsigned int(1) )
{
}


Releasable::~Releasable()
{
    release();
}


void Releasable::release()
{
    if ( (-- *ref_count_) <= 0 )
        finalize();
}


void Releasable::add_ref()
{
    *ref_count_ ++;
}


int Releasable::ref_count()
{
    return *ref_count_;
}
