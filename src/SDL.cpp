#if defined(HAVE_CONFIG_H)
#   include "config.h"
#endif  /* defined(HAVE_CONFIG_H) */

#include <jade/SDL.hxx>
USING_NAMESPACE_JADE;

const Uint32 SDL::INIT_AUDIO = SDL_INIT_AUDIO;
const Uint32 SDL::INIT_VIDEO = SDL_INIT_VIDEO;
const Uint32 SDL::INIT_TIMER = SDL_INIT_TIMER;


void SDL::init(Uint32 flags)
{
    if ( SDL_Init( flags ) < 0 ) {
        //throw Error( SDL_GetError() );
    }
}


void SDL::quit()
{
    SDL_Quit();
}
// Local Variables:
//   coding: utf-8
// End:
