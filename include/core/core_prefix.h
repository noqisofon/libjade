#ifndef jade_core_prefix_h
#define jade_core_prefix_h

#define   BEGIN_NAMESPACE_CORE     namespace core {
#define   END_NAMESPACE_CORE       }

#define   USING_NAMESPACE_CORE     using namespace core

#endif  /* jade_core_prefix_h */
// Local Variables:
//   coding: utf-8
// End:
