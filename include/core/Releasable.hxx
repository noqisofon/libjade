#ifndef core_Releasable_hxx
#define core_Releasable_hxx

#include "core_prefix.h"


BEGIN_NAMESPACE_CORE


class Releasable
{
 public:
    /**
     *
     */
    Releasable();


    virtual ~Releasable();
    
 public:
    /**
     * 
     */
    virtual void release();


    /**
     * 
     */
    virtual void add_ref();


    /**
     * 
     */
    virtual int ref_count() const;

 protected:
    /**
     * 
     */
    virtual void finalize() = 0;

 private:
    unsigned int* ref_count_;
};


END_NAMESPACE_CORE


#endif  /* core_Releasable_hxx */
// Local Variables:
//   coding: utf-8
// End:
