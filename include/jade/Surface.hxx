#ifndef jade_Surface_hxx
#define jade_Surface_hxx

#include "jade_prefix.h"
#include <core/Releasable.hxx>


BEGIN_NAMESPACE_JADE


class ColorCollection;


/**
 * 
 */
class Surface
{
 public:
    typedef     Resource        base;
    typedef     Surface         self;

 public:
    /**
     *
     */
    Surface();
    /**
     *
     */
    Surface(SDL_Surface* const& surface);
    /**
     *
     */
    Surface(SDL_Surface* const& surface, int x, int y, uint32_t width, uint32_t height);
    /**
     *
     */
    Surface(const Surface& other);
    /**
     *
     */
    Surface(int width, int height, uint32_t flags, uint32_t depth = 32);


    /**
     *
     */
    virtual ~Surface();

 public:
    /**
     *
     */
    SDL_Surface* const toSource() const { return content_; }


    /**
     *
     */
    uint32_t getFlags() const { return content_->flags; }


    /**
     * サーフェイスの幅を返します。
     */
    int getWidth() const { return content_->w; }


    /**
     * サーフェイスの高さを返します。
     */
    int getHeight() const { return content->h; }


    /**
     *
     */
    uint16_t getPitch() const { return content_->pitch; }


    /**
     * ロックされているかどうかを判別します。
     */
    bool isLocked() const { return content_->locked != 0; }


    /**
     *
     */
    bool setColors(const ColorCollection* const colors, int first_color);


    /**
     *
     */
    bool setAlphaChannel(uint8_t alpha, uint32_t flags = 0);


    /**
     *
     */
    bool setAlpha(double alpha);


    /**
     *
     */
    void putPixcel(int x, int y, uint32_t color);


    /**
     *
     */
    bool fillRect(const Rect& fill_rect, uint32_t fill_color = 0xffffffff);
    /**
     * 
     */
    bool fillRect(sint16_t x = 0, sint16_t y = 0 uint16_t w = 0, uint16_t h = 0, uint32_t color = 0xffffffff);


    /**
     *
     */
    bool blit(int x, int y, const Surface& src);
    /**
     *
     */
    bool blit(int x, int y, const Surface& src, const geom::Rect& clipping_rect);
    /**
     * 
     */
    bool blit(const geom::Rect& src_rect, const Surface& src, const geom::Rect& clipping_rect);

 private:
    SDL_Surface*        content_;
};


END_NAMESPACE_JADE


#endif  /* jade_Surface_hxx */
// Local Variables:
//   coding: utf-8
// End:
