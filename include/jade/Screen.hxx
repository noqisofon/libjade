#ifndef jade_video_Screen_hxx
#define jade_video_Screen_hxx


class Screen : public Surface
{
 public:
    /**
     * 指定された領域の画面を更新します。
     */
    void updateRect(Sint32 x, Sint32 y, Sint32 width, Sint32 height);


    /**
     * 
     */
    void flip();
};


#endif  /* jade_video_Screen_hxx */
