#ifndef jade_jade_prefix_h
#define jade_jade_prefix_h

#if defined(HAVE_LIBSDL) && HAVE_LIBSDL == 1
#   include <SDL/SDL.h>
#endif  /* defined(HAVE_LIBSDL) && HAVE_LIBSDL == 1 */

#if defined(HAVE_LIBSDL_IMAGE) && HAVE_LIBSDL_IMAGE == 1
#   include <SDL/SDL_image.h>
#endif  /* defined(HAVE_LIBSDL_IMAGE) && HAVE_LIBSDL_IMAGE == 1 */


#define   BEGIN_NAMESPACE_JADE     namespace jade {
#define   END_NAMESPACE_JADE       }


#define   USING_NAMESPACE_JADE     using namespace jade


#endif  /* jade_jade_prefix_h */
// Local Variables:
//   coding: utf-8
// End:
