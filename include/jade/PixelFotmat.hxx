#ifndef jade_PixcelFormat_hxx
#define jade_PixcelFormat_hxx

#include "jade_prefix.h"


BEGIN_NAMESPACE_JADE

/**
 * 
 */
class PixelFormat
{
 public:
    /**
     * 
     */
    PixelFormat();

 public:
    /**
     *各ピクセルを表すのに使われるビット数を返します。
     */
    Uint8 getBitsPerPixel() const;


    /**
     * 各ピクセルを表すのに使われるバイト数を返します。
     */
    Uint8 getBytesPerPixel() const;


    /**
     * 
     */
    Uint32 getRMask() const;


    /**
     * 
     */
    Uint32 getGMask() const;


    /**
     * 
     */
    Uint32 getBMask() const;


    /**
     * 
     */
    Uint32 getAMask() const;

 private:
    SDL_PixelFormat* format_;
};


END_NAMESPACE_JADE


#endif  /* jade_PixcelFormat_hxx */
// Local Variables:
//   coding: utf-8
// End:
