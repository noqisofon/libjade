#ifndef jade_SDL_hxx
#define jade_SDL_hxx

#include "jade_prefix.h"


BEGIN_NAMESPACE_JADE


/**
 * 
 */
class SDL
{
 public:
    /**
     * 
     */
    static void init(Uint32 flags);


    /**
     * 
     */
    static void quit();


    static const Uint32 INIT_AUDIO;
    static const Uint32 INIT_VIDEO;
    static const Uint32 INIT_TIMER;
    static const Uint32 INIT_CDROM;
    static const Uint32 INIT_JOYSTICK;
    static const Uint32 INIT_EVERYTHING;
    static const Uint32 INIT_NOPARACHUTE;
    static const Uint32 INIT_EVENTTHREAD;
};


END_NAMESPACE_JADE


#endif  /* jade_SDL_hxx */
// Local Variables:
//   coding: utf-8
// End:
